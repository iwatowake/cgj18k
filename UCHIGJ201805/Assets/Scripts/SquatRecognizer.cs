﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SquatRecognizer : MonoBehaviour
{
	public enum R_NO
	{
		INIT = 0,
		DETECT_DOWN,		//!< 下降判定
		DETECT_UP,			//!< 上昇判定
		DETECT_HORIZON,		//!< 水平判定

		DETECT_MOVE,		//!< 上下移動を判定
	}

	class AccelData
	{
		public Vector3 m_Accel;
		public float m_Time;
	}


	// Use this for initialization
	void Start ()
	{

	}
	
	int waitFrame = 0;
	// Update is called once per frame
	void Update ()
	{
		if(waitFrame < 10)
		{
			waitFrame++;
			return;
		}

		if( m_Rno != (int)R_NO.INIT )
		{
			// 加速度データ更新
			updateAccelData();
		}

		switch(m_Rno)
		{
			case (int)R_NO.INIT:
				init();
				m_Rno = (int)R_NO.DETECT_MOVE;
				break;

			case (int)R_NO.DETECT_DOWN:
				if( checkDown() == true )
				{
					m_Rno = (int)R_NO.DETECT_UP;
				}
				break;

			case (int)R_NO.DETECT_UP:
				if( checkUp() == true )
				{
					m_Rno = (int)R_NO.DETECT_HORIZON;
				}
				break;

			case (int)R_NO.DETECT_HORIZON:
				if( checkHorizon() == true )
				{
					m_Rno = (int)R_NO.DETECT_DOWN;

					// スクワット回数カウント
					m_SquatCount++;
				}
				break;

		　	case (int)R_NO.DETECT_MOVE:
				checkMove();
				break;

			default:
				break;
		}
	}

	void init()
	{
		// ジョイコンへのアタッチ
		attachJoycon();

		// 最初の加速度から、補正値を計算
		Vector3 accel = m_Joycon.GetAccel();

		// 基準の加速度
		Vector3 baseAccel = Vector3.down;

		m_AccHoseiQuat = Quaternion.FromToRotation(accel, baseAccel);
		//m_VecHoseiQuat = Quaternion.Inverse( m_Joycon.GetVector() );

		m_CurrentTime = 0.0f;
	}

	void updateAccelData()
	{
		m_CurrentAccel = m_AccHoseiQuat * m_Joycon.GetAccel();
		//m_CurrentVec = m_VecHoseiQuat * m_Joycon.GetVector();

		// 重力加速度を打ち消す
		m_CurrentAccel.y += 1.0f;

		// ボタン押しているだけログ出力して記録
		if( m_Joycon.GetButton(Joycon.Button.DPAD_DOWN) == true )
		{
			//Debug.Log(m_CurrentAccel);
		}

		// 加速度データを記録
		AccelData data = new AccelData();
		data.m_Accel = m_CurrentAccel;
		data.m_Time = m_CurrentTime;
		m_AccelDataList.Add(data);

		// 直近以外のデータはクリア
		for(int i=0; i<m_AccelDataList.Count; i++)
		{
			float dTime = m_CurrentTime - m_AccelDataList[i].m_Time;
			if( dTime > m_CaptureTime )
			{
				m_AccelDataList.RemoveAt(i);
				i--;
			}
		}

		// 平均値フィルタ
		m_MeanAccelDataList.Clear();
		int width = 1;
		for(int i=0, len=m_AccelDataList.Count; i<len; i++)
		{
			int count = 0;
			Vector3 sumAcc = Vector3.zero;
			for(int j=i-width; j<=i+width; j++)
			{
				if(j < 0) continue;
				if(j >= len) continue;

				count++;
				sumAcc += m_AccelDataList[j].m_Accel;
			}

			AccelData meanData = new AccelData();
			meanData.m_Time = m_AccelDataList[i].m_Time;
			meanData.m_Accel.x = sumAcc.x / (float)count;
			meanData.m_Accel.y = sumAcc.y / (float)count;
			meanData.m_Accel.z = sumAcc.z / (float)count;

			m_MeanAccelDataList.Add( meanData );
		}

		// ボタン押しているだけログ出力して記録
		if( m_Joycon.GetButton(Joycon.Button.DPAD_DOWN) == true )
		{
			//Debug.Log(m_MeanAccelDataList[m_MeanAccelDataList.Count-1].m_Accel);
		}

		m_CurrentTime += Time.deltaTime;
	}

	bool checkDown()
	{
		int countStartIndex = -1;
		for(int i=0; i<m_MeanAccelDataList.Count; i++)
		{
			if( m_MeanAccelDataList[i].m_Accel.y <= m_DownAccel &&
				m_MeanAccelDataList[i].m_Accel.y > m_DownAccelLimit &&
				(i < m_MeanAccelDataList.Count - 1) )
			{
				if( countStartIndex == -1 )
				{
					countStartIndex = i;
				}
			}
			else if( countStartIndex >= 0 )
			{
				// 下降判定をとっていた時間長さを計算
				float time = m_MeanAccelDataList[i].m_Time - m_MeanAccelDataList[countStartIndex].m_Time;
				if( time >= m_DownCheckTime )
				{
					return true;
				}

				countStartIndex = -1;
			}
		}

		return false;
	}

	bool checkUp()
	{
		int countStartIndex = -1;
		for(int i=0; i<m_MeanAccelDataList.Count; i++)
		{
			if( m_MeanAccelDataList[i].m_Accel.y >= m_UpAccel &&
				m_MeanAccelDataList[i].m_Accel.y < m_UpAccelLimit &&
				(i < m_MeanAccelDataList.Count - 1) )
			{
				if( countStartIndex == -1 )
				{
					countStartIndex = i;
				}
			}
			else if( countStartIndex >= 0 )
			{
				// 判定をとっていた時間長さを計算
				float time = m_MeanAccelDataList[i].m_Time - m_MeanAccelDataList[countStartIndex].m_Time;
				if( time >= m_UpCheckTime )
				{
					return true;
				}
				
				countStartIndex = -1;
			}
		}

		return false;
	}

	bool checkHorizon()
	{
		int countStartIndex = -1;
		for(int i=0; i<m_MeanAccelDataList.Count; i++)
		{
			float accel = Mathf.Abs(m_MeanAccelDataList[i].m_Accel.y);
			if( accel < m_HorizonAccel && (i < m_MeanAccelDataList.Count - 1) )
			{
				if( countStartIndex == -1 )
				{
					countStartIndex = i;
				}
			}
			else if( countStartIndex >= 0 )
			{
				// 判定をとっていた時間長さを計算
				float time = m_MeanAccelDataList[i].m_Time - m_MeanAccelDataList[countStartIndex].m_Time;
				if( time >= m_HorizonCheckTime )
				{
					return true;
				}
				
				countStartIndex = -1;
			}
		}

		return false;
	}

	void checkMove()
	{
		if(GameStateManager.Instance.GetStateName() != "発電フェーズ")
		{
			m_SquatPower = 0.0f;
			return;
		}

		// 加速度の大きさ
		float mag = m_Joycon.GetAccel().magnitude;
		
		// 重力加速度の大きさを引く
		mag -= 1.0f;
		if( mag < 0.5f )
		{
			mag = 0.0f;
		}
		else
		{
			float amp = 0.5f * mag / 10.0f + 0.5f;
			if( amp > 1.0f )
			{
				amp = 1.0f;
			}
			m_Joycon.SetRumble( 160, 320, amp, 100 );
		}

		float rate = 0.0f;
		if( m_SquatForce < m_MaxSquatForce )
		{
			rate = (m_MaxSquatForce - m_SquatForce) / m_MaxSquatForce + 0.1f;
		}
		else
		{
			rate = 0.1f;
		}
		m_SquatForce += mag * Time.deltaTime * rate;
		m_HagurumaPower += m_SquatForce;		// 歯車用
		m_SquatPower = mag * Time.deltaTime;	// ゲージ加算用

		// ボタン押しているだけログ出力して記録
		if( m_Joycon.GetButton(Joycon.Button.DPAD_DOWN) == true )
		{
			Debug.Log(mag);
		}
	}

	public void attachJoycon()
	{
		m_Joycon = InputManager.Instance.getJoycon(m_PlayerId);
	}

	private Joycon m_Joycon = null;
	private int m_Rno = (int)R_NO.INIT;
	private Quaternion m_AccHoseiQuat = Quaternion.identity;

	private List<AccelData> m_AccelDataList = new List<AccelData>();
	private List<AccelData> m_MeanAccelDataList = new List<AccelData>();
	private float m_CurrentTime = 0.0f;

	public InputManager.PLAYER m_PlayerId;

	private Vector3 m_CurrentAccel = Vector3.zero;
	//private Quaternion m_CurrentVec = Quaternion.identity;
	private float m_CaptureTime = 0.25f;		//!< データの保存時間 [sec]
	private float m_DownAccel = -0.4f;		//!< 下降とみなす加速度
	private float m_DownAccelLimit = -1.0f;
	private float m_DownCheckTime = 0.15f;

	private float m_UpAccel = 0.4f;			//!< 上昇とみなす加速度
	private float m_UpAccelLimit = 1.0f;
	private float m_UpCheckTime = 0.15f;
	
	private float m_HorizonAccel = 0.3f;
	private float m_HorizonCheckTime = 0.05f;
	private int m_SquatCount = 0;

	private float m_SquatForce = 0.0f;
	private float m_HagurumaPower = 0.0f;
	private float m_MaxSquatForce = 30.0f;
	public float m_SquatPower = 0.0f;
}
