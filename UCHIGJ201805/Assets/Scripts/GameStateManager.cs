﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameStateManager : SingletonMonoBehaviour<GameStateManager> {

	public readonly float REACTION_WAIT_SEC = 0.25f;
	private State currentState;
	public TeamStatus[] teamStatus = new TeamStatus[2];
	public int attackTeam = 0;
	public int deffenceTeam = 1;

	[SerializeField]
	private int DEFAULT_LIFE = 3;

	// Use this for initialization
	void Start () {
		InitTeamStatus();
		SetState(new TitleState(this));
	}
	
	// Update is called once per frame
	void Update () {
		currentState.Tick();
	}

	public void SetState(State state){
		if(currentState != null){
			currentState.OnStateExit();
		}

		currentState = state;

		if(currentState != null){
			currentState.OnStateEnter();
		}
	}

	public string GetStateName(){
		return currentState.GetName();
	}

	public void InitTeamStatus(){
		teamStatus[0] = new TeamStatus(DEFAULT_LIFE);
		teamStatus[1] = new TeamStatus(DEFAULT_LIFE);
	}

/* 	private void OnGUI(){
		var style = GUI.skin.GetStyle( "label" );
        style.fontSize = 16;
		GUI.contentColor = new Color(0,0,0);
		GUILayout.BeginHorizontal(GUILayout.Width(0));
		GUILayout.BeginVertical(GUILayout.Width(0));
		GUILayout.Label(currentState.GetName());
	} */
}
