﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputManager : SingletonMonoBehaviour<InputManager> {

	public enum TEAM {
		TEAM1 = 0,
		TEAM2
	}

	public enum PLAYER {
		PLAYER1 = 0,    // TEAM1 Left
		PLAYER2,        // TEAM1 Right
		PLAYER3,        // TEAM2 Left
		PLAYER4,        // TEAM2 Right
	}

	public enum DIRECTION {
		NONE,
		UP,
		RIGHT,
		LEFT,
		DOWN,
	}

    // variables
    [SerializeField]
    private DIRECTION[] directions;
    [SerializeField]
    private DIRECTION[] lastDirections;
    private List<Joycon> joycons;
    private int[] playerIdToJoyconIndexTbl = new int[] {0,1,2,3};


    public float directionThreshold = 20f;
    public float accelThreshold = 3f;

    public void Start()
    {
        if(JoyconManager.Instance == null)
        {
            Debug.LogError("JoyconManager is not exist.");
        }

        joycons = JoyconManager.Instance.j;

        int leftJcCount = 0;
        int rightJcCount = 0;
        // player 割りあて
        for (int i = 0; i < joycons.Count;i++)
        {
            var jc = joycons[i];
            if (jc.isLeft)
            {
                playerIdToJoyconIndexTbl[i] = 1 + leftJcCount * 2;
                leftJcCount++;
            }
            else
            {
                playerIdToJoyconIndexTbl[i] = rightJcCount * 2;
                rightJcCount++;
            }
        }


        directions = new DIRECTION[rightJcCount];
        lastDirections = new DIRECTION[rightJcCount];

    }

    public void Update()
    {
      for(int i = 0; i < directions.Length; i++)
        {
            // Player2,4がdirection
            directions[i] = judgeDirection(getJoycon((PLAYER)System.Enum.ToObject(typeof(PLAYER), i*2)));

            if(directions[i] != DIRECTION.NONE)
            {
                if (lastDirections[i] != directions[i])
                {
                    Debug.LogFormat("Player{0}:{1}", (1 + i * 2).ToString(),System.Enum.GetName(typeof(DIRECTION), directions[i]));
                }
                lastDirections[i] = directions[i];
            }

        }
    }


    public bool IsAnyButtonPressed(PLAYER player){
		return Input.anyKeyDown;
	}

	public bool IsDoneChargeMove(TEAM team){
		switch(team){
			case TEAM.TEAM1:
				return Input.GetKeyDown(KeyCode.Z);
			case TEAM.TEAM2:
				return Input.GetKeyDown(KeyCode.M);
			default:
				return false;
		}
	}

    public DIRECTION GetActDirectionDev(int team){
        return GetActDirectionDev((TEAM)team);
    }

	public DIRECTION GetActDirectionDev(TEAM team){
        DIRECTION dir = DIRECTION.NONE;
		switch(team){
			case TEAM.TEAM1:
                //dir = judgeDirection(getJoycon(PLAYER.PLAYER2));
                
				if(Input.GetKey(KeyCode.W)) dir = DIRECTION.UP;
				if(Input.GetKey(KeyCode.D)) dir = DIRECTION.RIGHT;
				if(Input.GetKey(KeyCode.A)) dir = DIRECTION.LEFT;
				if(Input.GetKey(KeyCode.S)) dir = DIRECTION.DOWN;
                break;
			case TEAM.TEAM2:
                //dir = judgeDirection(getJoycon(PLAYER.PLAYER4));

				if(Input.GetKey(KeyCode.I)) dir = DIRECTION.UP;
				if(Input.GetKey(KeyCode.L)) dir = DIRECTION.RIGHT;
				if(Input.GetKey(KeyCode.J)) dir = DIRECTION.LEFT;
				if(Input.GetKey(KeyCode.K)) dir = DIRECTION.DOWN;
                break;
		}

        return dir;
	}

    public DIRECTION GetActDirection(int team){
        return GetActDirection((TEAM)team);
    }

    public DIRECTION GetActDirection(TEAM team){
        if(team == TEAM.TEAM1)
            return judgeDirection(getJoycon(PLAYER.PLAYER1));
        if(team == TEAM.TEAM2)
            return judgeDirection(getJoycon(PLAYER.PLAYER3));

        return DIRECTION.NONE;
    }

    public Joycon getJoycon(PLAYER player) {
        return joycons[playerIdToJoyconIndexTbl[(int)player]];
    }
	
	public float GetSquatPower(TEAM team){
		switch(team){
			case TEAM.TEAM1:
				return m_Team1Squat.m_SquatPower;
			case TEAM.TEAM2:
				return m_Team2Squat.m_SquatPower;
			default:
				return 0.0f;
		}
	}

    private DIRECTION judgeDirection(Joycon jc)
    {
        Vector3 gyro = jc.GetGyro();
        float accelMagnitude = jc.GetAccel().magnitude;
        if(accelMagnitude < accelThreshold)
        {
            return DIRECTION.NONE;
        }



        if(gyro.z > directionThreshold) {
            return DIRECTION.RIGHT;
        }else if(gyro.z < -directionThreshold)
        {
            return DIRECTION.LEFT;
        }else if(gyro.y > directionThreshold)
        {
            return DIRECTION.UP;
        }else if (gyro.y < -directionThreshold)
        {
            return DIRECTION.DOWN;
        }
        return DIRECTION.NONE;
    }

	public SquatRecognizer m_Team1Squat = null;
	public SquatRecognizer m_Team2Squat = null;
}
