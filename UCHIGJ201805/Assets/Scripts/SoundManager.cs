﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SoundManager : SingletonMonoBehaviour<SoundManager>
{
	public enum BGM_ID
	{
		BGM_SQUAT = 0,
		BGM_BATTLE,
		BGM_TITLE,
		BGM_INVALID = -1,
	}

	public enum SE_ID
	{
		SE_DYNAMO = 0,	//!< 発電
		
		SE_COUNT_3,
		SE_COUNT_2,
		SE_COUNT_1,
		SE_COUNT_0,

		SE_ATTACK,
		SE_GUARD,

		SE_YOU_WIN_1,
		SE_YOU_WIN_2,
		SE_YOU_WIN_3,
		SE_YOU_WIN_4,
		SE_YOU_WIN_5,
		SE_YOU_WIN_6,

		SE_GONG,

		SE_FLASH,

		SE_INVALID = -1,
	}

	public const int BGM_STREAM_NUM = 2;
	public const int SE_STREAM_NUM = 8;

	void Awake()
	{
		for(int i=0; i<m_BgmAudioSource.Length; i++)
		{
			m_BgmAudioSource[i] = new BgmStream();
			m_BgmAudioSource[i].m_AudioSource = gameObject.AddComponent<AudioSource>();
			m_BgmAudioSource[i].m_AudioSource.loop = true;
		}
		for(int i=0; i<m_SeAudioSource.Length; i++)
		{
			m_SeAudioSource[i] = new BgmStream();
			m_SeAudioSource[i].m_AudioSource = gameObject.AddComponent<AudioSource>();
		}
	}

	// Use this for initialization
	void Start ()
	{
		
	}
	
	// Update is called once per frame
	void Update ()
	{
		for(int i=0; i<BGM_STREAM_NUM; i++)
		{
			m_BgmAudioSource[i].update();
		}
		for(int i=0; i<SE_STREAM_NUM; i++)
		{
			m_SeAudioSource[i].update();
		}
	}

	//! BGM再生
	public void playBgm(BGM_ID bgm_id)
	{
		if( m_BgmAudioSource[m_CurrentBgmStreamNo].isPlay() == true )
		{
			m_BgmAudioSource[m_CurrentBgmStreamNo].requestFadeOut(1.0f);

			swtichBgmStream();
		}

		m_BgmAudioSource[m_CurrentBgmStreamNo].requestPlay( m_AudioClips[(int)bgm_id]);
	}

	//! BGMフェードイン
	public void fadeinBgm(BGM_ID bgm_id, float fadeTime)
	{
		if( m_BgmAudioSource[m_CurrentBgmStreamNo].isPlay() == true )
		{
			m_BgmAudioSource[m_CurrentBgmStreamNo].requestFadeOut(1.0f);

			swtichBgmStream();
		}

		m_BgmAudioSource[m_CurrentBgmStreamNo].requestFadeIn(m_AudioClips[(int)bgm_id], fadeTime);
	}

	//! BGMフェードアウト
	public void fadeoutBgm(float fadeTime)
	{
		// すでにBGMがなっていたらフェードさせる
		if( m_BgmAudioSource[m_CurrentBgmStreamNo].isPlay() == true )
		{
			m_BgmAudioSource[m_CurrentBgmStreamNo].requestFadeOut(fadeTime);
		}
	}

	//! BGM停止
	public void stopBgm()
	{
		if( m_BgmAudioSource[m_CurrentBgmStreamNo].isPlay() == true )
		{
			m_BgmAudioSource[m_CurrentBgmStreamNo].stop();
		}
	}

	//! SE再生
	//! @return	操作用のID
	public int playSe(SE_ID se_id, float pitch = 1.0f)
	{
		int emptySlotIdx = -1;
		// 空いているストリームを探す
		for(int i=0; i<SE_STREAM_NUM; i++)
		{
			if( m_SeAudioSource[i].isPlay() == false )
			{
				emptySlotIdx = i;
				break;
			}
		}

		if( emptySlotIdx == -1 )
		{
			// 空きがなかったので、ランダムで強制停止
			emptySlotIdx = Random.Range(0, SE_STREAM_NUM);
			m_SeAudioSource[emptySlotIdx].stop();
		}

		return m_SeAudioSource[emptySlotIdx].requestPlay(m_SeClips[(int)se_id], pitch);
	}

	//! SE停止
	public void stopSe(int handleId)
	{
		int targetIdx = -1;
		for(int i=0; i<SE_STREAM_NUM; i++)
		{
			if( m_SeAudioSource[i].getHandleId() == handleId )
			{
				targetIdx = i;
				break;	
			}
		}

		if( targetIdx == -1 )
		{
			return;
		}

		m_SeAudioSource[targetIdx].stop();
	}

	//! SE再生中か
	public bool isPlaySe(int handleId)
	{
		for(int i=0; i<SE_STREAM_NUM; i++)
		{
			if( m_SeAudioSource[i].getHandleId() == handleId )
			{
				if( m_SeAudioSource[i].isPlay() == true )
				{
					return true;
				}

				break;
			}
		}

		return false;
	}

	private void swtichBgmStream()
	{
		m_CurrentBgmStreamNo++;
		if( m_CurrentBgmStreamNo >= BGM_STREAM_NUM )
		{
			m_CurrentBgmStreamNo = 0;
		}	
	}
	private void switchSeStream()
	{
		m_CurrentSeStreamNo++;
		if( m_CurrentSeStreamNo >= SE_STREAM_NUM )
		{
			m_CurrentSeStreamNo = 0;
		}
	}

	public int generateHandleId()
	{
		m_CurrentHandleId++;
		return m_CurrentHandleId;
	}

	public BgmStream[] m_BgmAudioSource = new BgmStream[BGM_STREAM_NUM];
	public BgmStream[] m_SeAudioSource = new BgmStream[SE_STREAM_NUM];
	public AudioClip[] m_AudioClips;
	public AudioClip[] m_SeClips;

	public int m_CurrentBgmStreamNo = 0;
	private int m_CurrentSeStreamNo = 0;

	private int m_CurrentHandleId = 0;
}

public class SoundStream
{
	public AudioSource m_AudioSource;

	public virtual void update()
	{
	}
}

public class BgmStream : SoundStream
{
	public enum R_NO
	{
		STOP,
		PLAY,
		FADE_IN,
		FADE_OUT,
	}

	public int requestPlay(AudioClip clip, float pitch = 1.0f)
	{
		if( isPlay() == true )
		{
			return -1;
		}

		m_Rno = R_NO.PLAY;
		m_AudioSource.clip = clip;
		m_AudioSource.volume = m_Volume;
		m_AudioSource.pitch = pitch;
		m_AudioSource.Play();

		m_HandleId = SoundManager.Instance.generateHandleId();

		return m_HandleId;
	}

	public void requestFadeOut(float fadeTime)
	{
		if( m_Rno == R_NO.FADE_OUT || m_Rno == R_NO.STOP )
		{
			return;
		}

		m_Rno = R_NO.FADE_OUT;
		m_FadeTime = fadeTime;
		m_FadeTimer = 0.0f;
	}

	public int requestFadeIn(AudioClip clip, float fadeTime)
	{
		if( isPlay() == true )
		{
			return -1;
		}

		m_Rno = R_NO.FADE_IN;
		m_FadeTime = fadeTime;
		m_FadeTimer = 0.0f;
		m_HandleId = SoundManager.Instance.generateHandleId();

		m_AudioSource.clip = clip;
		m_AudioSource.volume = 0.0f;
		m_AudioSource.Play();

		return m_HandleId;
	}

	public void stop()
	{
		m_Rno = R_NO.STOP;
		m_AudioSource.Stop();
	}

	public override void update()
	{
		switch(m_Rno)
		{
			case R_NO.PLAY:
				updatePlay();
				break;
			case R_NO.FADE_IN:
				updateFadeIn();
				break;
			case R_NO.FADE_OUT:
				updateFadeOut();
				break;
			default:
				break;
		}
	}

	public bool isPlay()
	{
		if( m_Rno == R_NO.PLAY || m_Rno == R_NO.FADE_IN || m_Rno == R_NO.FADE_OUT )
		{
			return true;
		}

		return false;
	}

	public bool isFadeOut()
	{
		if( m_Rno == R_NO.FADE_OUT )
		{
			return true;
		}
		return false;
	}

	public bool isStop()
	{
		if( m_Rno == R_NO.STOP )
		{
			return true;
		}
		return false;
	}

	public void updatePlay()
	{
		if( m_AudioSource.isPlaying == false )
		{
			stop();
		}
	}

	public void updateFadeIn()
	{
		m_FadeTimer += Time.deltaTime;

		float volume = m_Volume * m_FadeTimer / m_FadeTime;
		if( m_FadeTimer >= m_FadeTime )
		{
			m_Rno = R_NO.PLAY;
			volume = m_Volume;
		}

		m_AudioSource.volume = volume;

	}

	public void updateFadeOut()
	{
		m_FadeTimer += Time.deltaTime;

		float volume = 1.0f - m_Volume * m_FadeTimer / m_FadeTime;
		if( m_FadeTimer >= m_FadeTime )
		{
			stop();
		}

		m_AudioSource.volume = volume;
	}

	public R_NO getRoutine()
	{
		return m_Rno;
	}

	public int getHandleId()
	{
		return m_HandleId;
	}

	private R_NO m_Rno = R_NO.STOP;
	private float m_FadeTime = 0.0f;
	private float m_FadeTimer = 0.0f;
	private float m_Volume = 1.0f;
	private int m_HandleId = 0;
}

public class SeStream : SoundStream
{
}
