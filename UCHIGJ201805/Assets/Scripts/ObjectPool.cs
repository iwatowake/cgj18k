﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPool : SingletonMonoBehaviour<ObjectPool> {
	public GameObject Obj_UICharge;
	public GameObject Obj_UICountDown;
	public GameObject Obj_UICombat;
	public GameObject Obj_UICombatGuide;
	public GameObject Obj_UIResult;
	public GameObject Obj_UILife;
	public GameObject OBJ_UIFlash;
    public GameObject Obj_UITitle;
}
