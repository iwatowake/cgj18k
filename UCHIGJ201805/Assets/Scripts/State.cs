﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DG.Tweening;

public abstract class State
{
	//protected Character character;
	protected GameStateManager manager;
	protected string name;

	public abstract void Tick();

	public virtual void OnStateEnter() { }
	public virtual void OnStateExit() { }

	public State(GameStateManager manager)
	{
		this.manager = manager;
	}

	public string GetName()
	{
		return name;
	}
}

public class ExampleState : State
{
	GameObject mObj;

	public ExampleState(GameStateManager manager) : base(manager)
	{
	}

	public override void Tick()
	{
		if(IsFinishState()){
			manager.SetState(new ExampleState2(manager));
		}
	}

	public override void OnStateEnter(){
		mObj = new GameObject("hoge1");
	}

	public override void OnStateExit(){
		GameObject.Destroy(mObj);
	}

	private bool IsFinishState()
	{
		return Input.GetKeyDown(KeyCode.Alpha1);
	}
}
public class ExampleState2 : State
{
	GameObject mObj;

	public ExampleState2(GameStateManager manager) : base(manager)
	{
	}

	public override void Tick()
	{
		if(IsFinishState()){
			manager.SetState(new ExampleState(manager));
		}
	}

	public override void OnStateEnter(){
		mObj = new GameObject("hoge2");
	}

	public override void OnStateExit(){
		GameObject.Destroy(mObj);
	}

	private bool IsFinishState()
	{
		return Input.GetKeyDown(KeyCode.Alpha2);
	}
}

/// <summary>
/// 発電フェーズ
/// </summary>
public class ChargeState : State 
{
	UI_Charge ui_charge;
	float charge_se_wait_time = 0.0f;
	const int MAX_CHARGE = 50;

	public ChargeState(GameStateManager manager) : base(manager)
	{
		name = "発電フェーズ";
	}

	public override void Tick(){
		// 入力に応じてエネルギーを加算、UIをアップデート
		// いずれかのプレイヤーのエネルギーが一定値に届いたらGameManagerに攻撃PLを設定し、カウントダウン状態へ
		float team1_add = InputManager.Instance.GetSquatPower(InputManager.TEAM.TEAM1);
		float team2_add = InputManager.Instance.GetSquatPower(InputManager.TEAM.TEAM2);
		// ライフ差に応じて加算に倍率をかける
		int dLife = manager.teamStatus[0].life - manager.teamStatus[1].life;
		if( dLife > 0 ){
			// チーム２が負けているので、チーム２の発電量に加算
			if( dLife == 1 ){
				team2_add *= 1.1f;
			}
			else if( dLife == 2 ){
				team2_add *= 1.4f;
			}
		}
		else if( dLife < 0 ){
			// チーム１が負けているので、チーム２の発電量に加算
			if( dLife == -1 ){
				team1_add *= 1.1f;
			}
			else if( dLife == -2 ){
				team1_add *= 1.4f;
			}
		}

		manager.teamStatus[0].chargeAmount += team1_add;
		manager.teamStatus[1].chargeAmount += team2_add;

		// 発電SE
		{
			float addPower = InputManager.Instance.GetSquatPower(InputManager.TEAM.TEAM1) + 
							InputManager.Instance.GetSquatPower(InputManager.TEAM.TEAM2);
			if( addPower >= 0.1f ){
				bool bPlayNow = false;
				if( charge_se_wait_time > 0.0f ){
					charge_se_wait_time -= Time.deltaTime;
				}
				else{
					charge_se_wait_time = 0.02f;

					float pitch0 = manager.teamStatus[0].chargeAmount / (float)MAX_CHARGE * 1.25f + 0.5f;
					float pitch1 = manager.teamStatus[1].chargeAmount / (float)MAX_CHARGE * 1.25f + 0.5f;
					if( pitch0 < pitch1 ){
						pitch0 = pitch1;
					}
					SoundManager.Instance.playSe( SoundManager.SE_ID.SE_DYNAMO, pitch0 );
				}
			}
		}

		UpdateUI();

		if(IsCharged(0)){
			ResetAmount(0);
			ResetAmount(1);
			manager.attackTeam = 0;
			manager.deffenceTeam = 1;
			manager.SetState(new CountDownState(manager));
			return;
		}

		if(IsCharged(1)){
			ResetAmount(1);
			ResetAmount(0);
			manager.attackTeam = 1;
			manager.deffenceTeam = 0;
			manager.SetState(new CountDownState(manager));
			return;
		}
	}

	private void UpdateUI(){
		ui_charge.SetAmount(0, manager.teamStatus[0].chargeAmount/MAX_CHARGE);
		ui_charge.SetAmount(1, manager.teamStatus[1].chargeAmount/MAX_CHARGE);
	}

	private bool IsCharged(int teamNumber){
		return manager.teamStatus[teamNumber].chargeAmount >= MAX_CHARGE;
	}

	private void ResetAmount(int teamNumber){
		manager.teamStatus[teamNumber].chargeAmount = 0;
	}

	private void ReduceAmount(int teamNumber){
		manager.teamStatus[teamNumber].chargeAmount /= 2.0f;
	}

	public override void OnStateEnter(){
		// ゲージUI生成、初期値をセット
		var objUI = GameObject.Instantiate(ObjectPool.Instance.Obj_UICharge);
		ui_charge = objUI.GetComponent<UI_Charge>();
		UpdateUI();

		SoundManager.Instance.playBgm( SoundManager.BGM_ID.BGM_SQUAT );
	}

	public override void OnStateExit(){
		// ゲージUI破棄
		GameObject.Destroy(ui_charge.gameObject);

		SoundManager.Instance.fadeoutBgm(1.0f);
	}
}

/// <summary>
/// カウントダウンフェーズ
/// </summary>
public class CountDownState : State 
{
	UI_CountDown ui_countdown;

	public CountDownState(GameStateManager manager) : base(manager)
	{
		name = "カウントダウンフェーズ";
	}

	public override void Tick(){
		// カウントダウン終了待機 ⇒ 戦闘状態へ
		if(ui_countdown.IsCountEnd()){
			manager.SetState(new CombatState(manager));
		}
	}

	public override void OnStateEnter(){
		// カウントダウンUI生成
		var objUI = GameObject.Instantiate(ObjectPool.Instance.Obj_UICountDown);
		ui_countdown = objUI.GetComponent<UI_CountDown>();

		// 操作指示UI生成
		objUI = GameObject.Instantiate(ObjectPool.Instance.Obj_UICombatGuide);
		objUI.GetComponent<UI_CombatGuide>().SetRoll(manager.attackTeam, manager.deffenceTeam);

		// 戦闘BGMを再生
		SoundManager.Instance.playBgm(SoundManager.BGM_ID.BGM_BATTLE);

		// 攻撃側のコントローラを振動させる
		if(JoyconManager.Instance.j.Count == 4){
			var jc = InputManager.Instance.getJoycon((InputManager.PLAYER)(manager.attackTeam * 2));
			jc.SetRumble(160, 320, 1.0f, 200);
		}else{
			var jc = InputManager.Instance.getJoycon((InputManager.PLAYER)manager.attackTeam);
			jc.SetRumble(160, 320, 1.0f, 200);
		}
	}

	public override void OnStateExit(){
		// カウントダウンUI破棄
		GameObject.Destroy(ui_countdown.gameObject);
	}
}

/// <summary>
/// 戦闘フェーズ
/// </summary>
public class CombatState : State 
{
	enum COMBAT_SUBSTATE{
		WAIT_ATTACK = 0,
		ADJUST_ATTACK,
		WAIT_REACTION,
		ADJUST_REACTION,
		SHOW_SHORTRESULT,
		WAIT_SHORTRESULT
	}

	UI_Combat ui_combat;
	UI_Flash ui_flash;
	float timer = 0.0f;
	COMBAT_SUBSTATE subState = COMBAT_SUBSTATE.WAIT_ATTACK;

	InputManager.DIRECTION[] direction = new InputManager.DIRECTION[]{InputManager.DIRECTION.NONE, InputManager.DIRECTION.NONE};

	public CombatState(GameStateManager manager) : base(manager)
	{
		name = "戦闘フェーズ";
	}

	public override void Tick(){
		switch(subState){
			case COMBAT_SUBSTATE.WAIT_ATTACK:
				if(InputManager.Instance.GetActDirection(manager.attackTeam) != InputManager.DIRECTION.NONE){
					direction[manager.attackTeam] = InputManager.Instance.GetActDirection(manager.attackTeam);
					subState = COMBAT_SUBSTATE.ADJUST_ATTACK;

					// 白明滅
					ui_flash.requestFlash(0.05f, 0.05f, 0.05f);
					SoundManager.Instance.playSe(SoundManager.SE_ID.SE_FLASH);
				}
				/*
				if(InputManager.Instance.GetActDirectionDev(manager.attackTeam) != InputManager.DIRECTION.NONE){
					direction[manager.attackTeam] = InputManager.Instance.GetActDirectionDev(manager.attackTeam);
					subState = COMBAT_SUBSTATE.ADJUST_ATTACK;
				}
				*/
				break;
			case COMBAT_SUBSTATE.ADJUST_ATTACK:
				// ウェイト設ける？
				//direction[manager.attackTeam] = InputManager.Instance.GetActDirection(manager.attackTeam);
				subState = COMBAT_SUBSTATE.WAIT_REACTION;
				break;
			case COMBAT_SUBSTATE.WAIT_REACTION:
				timer += Time.deltaTime;
				if(timer >= manager.REACTION_WAIT_SEC) {
					subState = COMBAT_SUBSTATE.SHOW_SHORTRESULT;
				}
				
 				if(InputManager.Instance.GetActDirection(manager.deffenceTeam) != InputManager.DIRECTION.NONE){
					direction[manager.deffenceTeam] = InputManager.Instance.GetActDirection(manager.deffenceTeam);
					subState = COMBAT_SUBSTATE.ADJUST_REACTION;
				}
				/*
				if(InputManager.Instance.GetActDirectionDev(manager.deffenceTeam) != InputManager.DIRECTION.NONE){
					direction[manager.deffenceTeam] = InputManager.Instance.GetActDirectionDev(manager.deffenceTeam);
					subState = COMBAT_SUBSTATE.ADJUST_REACTION;
				}
				*/
				break;
			case COMBAT_SUBSTATE.ADJUST_REACTION:
				// ウェイト設ける？
				//direction[manager.deffenceTeam] = InputManager.Instance.GetActDirection(manager.deffenceTeam);
				subState = COMBAT_SUBSTATE.SHOW_SHORTRESULT;
				break;
			case COMBAT_SUBSTATE.SHOW_SHORTRESULT:
				KillGuide();
				UpdateUI();
				var isHit = true;
				if(direction[manager.attackTeam] == InputManager.DIRECTION.RIGHT){
					isHit = direction[manager.deffenceTeam] != InputManager.DIRECTION.LEFT;
				}else if(direction[manager.attackTeam] == InputManager.DIRECTION.LEFT){
					isHit = direction[manager.deffenceTeam] != InputManager.DIRECTION.RIGHT;
				}else{
					isHit = direction[manager.deffenceTeam] != direction[manager.attackTeam];
				}

				// ライフを減らす
				if(isHit){
					var life = --manager.teamStatus[manager.deffenceTeam].life;
					Debug.Log("life: "+life);
					UI_Life.Instance.setLife(manager.deffenceTeam, life);
					ui_combat.ShakeArrow(manager.deffenceTeam);
					InputManager.Instance.getJoycon(manager.deffenceTeam-2 < 0 ? InputManager.PLAYER.PLAYER1 :  InputManager.PLAYER.PLAYER3);

					// 攻撃ヒットSE
					SoundManager.Instance.playSe( SoundManager.SE_ID.SE_ATTACK );
				}
				else{
					// ガード成功SE
					SoundManager.Instance.playSe( SoundManager.SE_ID.SE_GUARD );
				}

				// BGM OFF
				SoundManager.Instance.fadeoutBgm(0.5f);

				// 勝敗を何らかの形で表示（矢印吹っ飛ばすとか）

				timer = 0;
				subState = COMBAT_SUBSTATE.WAIT_SHORTRESULT;
				break;
			case COMBAT_SUBSTATE.WAIT_SHORTRESULT:
				timer += Time.deltaTime;
				if(timer < 1.5f) break;
				// 演出終了まで待機
				// ライフが残ってたらチャージへ戻る
				// ライフが切れてたらリザルトへ
				if(manager.teamStatus[manager.deffenceTeam].life <= 0){
				//if(true){
					manager.SetState(new ResultState(manager));
					// ライフUI破棄
					UI_Life.Instance.DestroySelf();
				}else{
					manager.SetState(new GameStartState(manager));
				}
				break;
		}
	}

	private void UpdateUI(){
		ui_combat.SetDirection(direction[0], direction[1]);
	}

	private void KillGuide(){
		var objUI = GameObject.Find("UI_CombatGuide(Clone)");
		GameObject.Destroy(objUI);
	}

	private void FlushInput(){
		direction[0] = InputManager.DIRECTION.NONE;
		direction[1] = InputManager.DIRECTION.NONE;
	}

	public override void OnStateEnter(){
		// 戦闘画面UI生成
		var objUI = GameObject.Instantiate(ObjectPool.Instance.Obj_UICombat);
		ui_combat = objUI.GetComponent<UI_Combat>();
		//ui_combat.SetRoll(manager.attackTeam, manager.deffenceTeam);

		// 白明滅UI
		objUI = GameObject.Instantiate(ObjectPool.Instance.OBJ_UIFlash);
		ui_flash = objUI.GetComponent<UI_Flash>();
	}

	public override void OnStateExit(){
		// 戦闘画面UI破棄
		GameObject.Destroy(ui_combat.gameObject);
	}
}

/// <summary>
/// リザルトフェーズ
/// </summary>
public class ResultState : State 
{
	UI_Result ui_result;

	public ResultState(GameStateManager manager) : base(manager)
	{
		name = "リザルトフェーズ";
	}

	public override void Tick(){
		// UI表示、ボタン入力待機
		if(Input.GetKeyDown(KeyCode.A) || InputManager.Instance.getJoycon(InputManager.PLAYER.PLAYER1).GetButtonDown(Joycon.Button.DPAD_RIGHT)) {
			manager.SetState(new TitleState(manager));
		}
	}

	public override void OnStateEnter(){
		// リザルトUI表示
		var objUI = GameObject.Instantiate(ObjectPool.Instance.Obj_UIResult);
		ui_result = objUI.GetComponent<UI_Result>();
		ui_result.ShowResultText(manager.attackTeam);

		// 背景アニメーション
		var objBG = GameObject.Find("background_"+(manager.attackTeam+1));
		objBG.transform.Translate(0,0,-1, Space.World);
		objBG.transform.DOScale(3, 0.75f).SetEase(Ease.InExpo);

		var range = Random.Range(1,6);
		SoundManager.SE_ID id;
		switch(range){
			default:
			case 1:
				id = SoundManager.SE_ID.SE_YOU_WIN_1;
				break;
			case 2:
				id = SoundManager.SE_ID.SE_YOU_WIN_2;
				break;
			case 3:
				id = SoundManager.SE_ID.SE_YOU_WIN_3;
				break;
			case 4:
				id = SoundManager.SE_ID.SE_YOU_WIN_4;
				break;
			case 5:
				id = SoundManager.SE_ID.SE_YOU_WIN_1;
				break;
			case 6:
				id = SoundManager.SE_ID.SE_YOU_WIN_6;
				break;
		}
		SoundManager.Instance.playSe(id);
	}

	public override void OnStateExit(){
		// 背景アニメーション
		var objBG = GameObject.Find("background_"+(manager.attackTeam+1));
		var sequence = DOTween.Sequence();
		sequence.Append(
			objBG.transform.DOScale(1, 0.5f).SetEase(Ease.OutExpo)
		);
		sequence.Append(
			objBG.transform.DOMoveZ(0,0.1f)
		);
		// リザルトUI破棄
		GameObject.Destroy(ui_result.gameObject);
	}
}

/// <summary>
/// タイトルフェーズ
/// </summary>
public class TitleState : State 
{
    // タイトルUI

    GameObject titleUIObj;
    // 各プレイヤーのボタンが押されたか

	public TitleState(GameStateManager manager) : base(manager)
	{
		name = "タイトルフェーズ";
	}

	public override void Tick(){
		// 全プレイヤーの入力待ち ⇒ ゲーム開始状態へ
		if(Input.GetKeyDown(KeyCode.Space) || InputManager.Instance.getJoycon(InputManager.PLAYER.PLAYER1).GetButtonDown(Joycon.Button.DPAD_RIGHT)){
			manager.SetState(new GameStartState(manager));
		}
	}

	public override void OnStateEnter(){
        // タイトルUI生成
        titleUIObj = GameObject.Instantiate(ObjectPool.Instance.Obj_UITitle);

        // 戦績データ初期化
        manager.InitTeamStatus();

		SoundManager.Instance.playBgm( SoundManager.BGM_ID.BGM_TITLE );
	}

	public override void OnStateExit(){
        // タイトルUI破棄
        GameObject.Destroy(titleUIObj);

		// ライフUI生成
		GameObject.Instantiate(ObjectPool.Instance.Obj_UILife);

		SoundManager.Instance.fadeoutBgm(0.5f);
	}
}

/// <summary>
/// ゲーム開始フェーズ
/// </summary>
public class GameStartState : State 
{
	UI_CountDown ui_countdown;
	int oldCount = 4;

	public GameStartState(GameStateManager manager) : base(manager)
	{
		name = "ゲーム開始フェーズ";
	}

	public override void Tick(){
		if(ui_countdown.IsCountEnd()){
			SoundManager.Instance.playSe(SoundManager.SE_ID.SE_GONG);

			manager.SetState(new ChargeState(manager));
		}
		else{
			int nowCount = ui_countdown.GetCount();
			if(nowCount < oldCount){
				oldCount = nowCount;
				switch(nowCount){
					case 3:
						SoundManager.Instance.playSe(SoundManager.SE_ID.SE_COUNT_3);
						break;
					case 2:
						SoundManager.Instance.playSe(SoundManager.SE_ID.SE_COUNT_2);
						break;
					case 1:
						SoundManager.Instance.playSe(SoundManager.SE_ID.SE_COUNT_1);
						break;		
				}
			}
		}
	}

	public override void OnStateEnter(){
		// カウントダウンUI生成
		var objUI = GameObject.Instantiate(ObjectPool.Instance.Obj_UICountDown);
		ui_countdown = objUI.GetComponent<UI_CountDown>();
		ui_countdown.countPerSec = 1.25f;
	}

	public override void OnStateExit(){
		// カウントダウンUI破棄
		GameObject.Destroy(ui_countdown.gameObject);
	}
}