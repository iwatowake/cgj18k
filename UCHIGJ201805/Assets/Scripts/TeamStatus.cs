﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TeamStatus {
    public float chargeAmount = 0;
    public int life = 0;
    public TeamStatus(int life)
    {
        this.life = life;
    }
}
