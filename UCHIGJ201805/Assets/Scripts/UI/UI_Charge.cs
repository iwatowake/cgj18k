﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Charge : MonoBehaviour
{

    [SerializeField]
    private GameObject[] amountObj;

    public void SetAmount(int team, float amount)
    {
        amountObj[team].GetComponent<RectTransform>().localScale = new Vector3(1, amount, 1);
    }
}
