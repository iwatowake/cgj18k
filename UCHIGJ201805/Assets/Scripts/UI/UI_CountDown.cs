﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_CountDown : MonoBehaviour {

	[SerializeField]
	private Text[] countTxt;
	float count = 3.0f;
	public float countPerSec = 1.0f;
	
	// Update is called once per frame
	void Update () {
		count -= Time.deltaTime * countPerSec;
		if(count > 0.0f){
			foreach(var txt in countTxt){
				txt.text = Mathf.CeilToInt(count).ToString();
			}
		}
	}

	public bool IsCountEnd(){
		return count <= 0.0f;
	}

	public int GetCount(){
		return Mathf.CeilToInt(count);
	}
}
