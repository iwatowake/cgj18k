﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Flash : MonoBehaviour
{
	enum R_NO
	{
		NONE = 0,
		FADE_IN,
		DISP,
		FADE_OUT,
	}

	// Use this for initialization
	void Start ()
	{
		m_Rno = R_NO.NONE;
	}
	
	// Update is called once per frame
	void Update ()
	{
		switch(m_Rno)
		{
			case R_NO.NONE:
				m_FlashImage.enabled = false;
				break;

			case R_NO.FADE_IN:
				updateFadeIn();
				break;

			case R_NO.DISP:
				updateDisp();
				break;

			case R_NO.FADE_OUT:
				updateFadeOut();
				break;
		}
	}

	public void requestFlash(float fadeInTime, float fadeOutTime, float dispTime)
	{
		m_Rno = R_NO.FADE_IN;
		m_FadeInTime = fadeInTime;
		m_DispTime = dispTime;
		m_FadeOutTIme = fadeOutTime;

		m_FlashImage.enabled = true;
		Color col = m_FlashImage.color;
		col.a = 0.0f;
		m_FlashImage.color = col;

		m_Timer = 0.0f;
	}

	void updateFadeIn()
	{
		m_Timer += Time.deltaTime;
		Color col = m_FlashImage.color;
		col.a = m_Timer / m_FadeInTime;
		if( col.a >= 1.0f )
		{
			col.a = 1.0f;
			m_Rno = R_NO.DISP;

			m_Timer = 0.0f;
		}

		m_FlashImage.color = col;
	}

	void updateDisp()
	{
		m_Timer += Time.deltaTime;
		if( m_Timer >= m_DispTime )
		{
			m_Rno = R_NO.FADE_OUT;
			m_Timer = 0.0f;
		}
	}

	void updateFadeOut()
	{
		m_Timer += Time.deltaTime;
		Color col = m_FlashImage.color;
		col.a = 1.0f - m_Timer / m_FadeOutTIme;
		if( col.a <= 0.0f )
		{
			col.a = 0.0f;
			m_Rno = R_NO.NONE;

			m_Timer = 0.0f;
		}

		m_FlashImage.color = col;
	}

	[SerializeField]
	private Image m_FlashImage = null;
	private R_NO m_Rno = R_NO.NONE;
	private float m_FadeInTime = 0.0f;
	private float m_FadeOutTIme = 0.0f;
	private float m_DispTime = 0.0f;
	private float m_Timer = 0.0f;
}
