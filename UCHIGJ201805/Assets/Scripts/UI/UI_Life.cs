﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Life : SingletonMonoBehaviour<UI_Life> {
	[SerializeField]
	private Sprite emptyHeartSprite;
	[SerializeField]
	private Sprite filledHeartSprite;

	[SerializeField]
	private Image[] lifeSprites_team1;
	[SerializeField]
	private Image[] lifeSprites_team2;

	private Dictionary<int, Image[]> lifeSprtes = new Dictionary<int, Image[]>();

	void Awake(){
		lifeSprtes.Add(0, lifeSprites_team1);
		lifeSprtes.Add(1, lifeSprites_team2);
	}

	private int life = 3;
	void Update(){
		if(Input.GetKeyDown(KeyCode.Alpha1))
			setLife(0, --life);
	}

	public void setLife(int team, int life){
		var lifeSlots = lifeSprtes[team].Length;
		var damaged = lifeSlots - life;

		for(var i=0; i<lifeSlots; i++){
			if(i < damaged){
				lifeSprtes[team][i].sprite = emptyHeartSprite;
			}else{
				lifeSprtes[team][i].sprite = filledHeartSprite;
			}
		}
	}

	public void DestroySelf(){
		GameObject.Destroy(this.gameObject);
	}
}
