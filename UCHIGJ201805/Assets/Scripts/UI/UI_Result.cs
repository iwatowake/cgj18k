﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UI_Result : MonoBehaviour {
	[SerializeField]
	private Text[] resultText;

	public void ShowResultText(int team){
		resultText[team].gameObject.SetActive(true);
	}
}
