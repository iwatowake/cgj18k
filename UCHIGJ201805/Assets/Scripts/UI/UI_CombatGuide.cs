﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UI_CombatGuide : MonoBehaviour {
	[SerializeField]
	private Text[] guideTxt;

	[SerializeField]
	private Text[] koubouTxt;

	[TextArea]
	public string GUIDETEXT_ATTACK;
	
	[TextArea]
	public string GUIDETEXT_DEFFENCE;

	[TextArea]
	public string GUIDETEXT_KOU;

	[TextArea]
	public string GUIDETEXT_BOU;

	public void SetRoll(int attackTeam, int deffenceTeam){
		guideTxt[attackTeam].text = GUIDETEXT_ATTACK;
		guideTxt[deffenceTeam].text = GUIDETEXT_DEFFENCE;
		
		koubouTxt[attackTeam].text = GUIDETEXT_KOU;
		koubouTxt[deffenceTeam].text = GUIDETEXT_BOU;
	}
}