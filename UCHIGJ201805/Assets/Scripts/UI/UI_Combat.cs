﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using DG.Tweening;

public class UI_Combat : MonoBehaviour {

	[SerializeField]
	private Text[] inputTxt;

	public void SetDirection(InputManager.DIRECTION direction_p1, InputManager.DIRECTION direction_p2)
	{
		inputTxt[0].text = GetArrowStr(direction_p1);
		inputTxt[1].text = GetArrowStr(direction_p2, true);
	}

	private string GetArrowStr(InputManager.DIRECTION direction, bool invertHorizontal = false){
		switch(direction){
			case InputManager.DIRECTION.UP:
			return "↑";
			case InputManager.DIRECTION.DOWN:
			return "↓";
			case InputManager.DIRECTION.RIGHT:
			return invertHorizontal ? "←" : "→";
			case InputManager.DIRECTION.LEFT:
			return invertHorizontal ? "→" : "←";
		}
		return "×";
	}

	public void ShakeArrow(int team){
		inputTxt[team].gameObject.transform.DOShakePosition(1.0f, 20, 50).SetEase(Ease.OutQuart);
	}
}